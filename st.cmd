require "mrfioc2" "2.3.1+6"

## Common envs
epicsEnvSet "ENGINEER" "ICS_HWI_WP04"
epicsEnvSet "TOP" "$(E3_CMD_TOP)/"

# [localhost]
## [CHESS] configuration (https://chess.esss.lu.se) and node specific settings
iocshLoad "$(TOP)/iocrc.iocsh"

# [common] e3 modules
## pre-settings
#epicsEnvSet "IOCNAME" "$(PEVR)"
#epicsEnvSet "IOCDIR" "./"
#epicsEnvSet "AS_TOP" "./"
epicsEnvSet "LOG_SERVER_NAME" "172.16.107.59"
# All ro and experts rw
#epicsEnvSet "PATH_TO_ASG_FILES" "$(auth_DIR)"
#epicsEnvSet "ASG_FILENAME" "allro_exprw.acf"
## body
$(CMNEN=)require essioc
$(CMNEN=)iocshLoad("$(essioc_DIR)/common_config.iocsh")

# [IOC] core
iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=$(PEVR),PCIID=$(PCIID),EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=$(PEVR)"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=$(PEVR),$(EVREVTARGS=)"

# [SIT] plugins
## ICSHWI-6250: SIT: ISrc-010Row:CnPw-U-005 FBIS-DLN01:Ctrl-EVR-01
$(SITMPSPLC="#")dbLoadRecords "evr-mps-plc-ess.db" "P=$(PEVR)"

# [user] settings - use this file to apply local customization
iocshLoad "$(TOP)/user.iocsh" "P=$(PEVR)"

# Auxiliary PVs to calculate periods
dbLoadRecords("$(E3_CMD_TOP)/db/evtperiod.template", "P=MEBT-010:, R=Ctrl-EVR-001:Aux-, EVRPREFIX=$(PEVR):")

iocInit

iocshLoad "$(mrfioc2_DIR)/evr.r.iocsh" "P=$(PEVR)"
$(EVRAMC2CLKEN=)iocshLoad "$(mrfioc2_DIR)/evrtclk.r.iocsh" "P=$(PEVR)"
iocshLoad "$(mrfioc2_DIR)/evrdlygen.r.iocsh" "P=$(PEVR),$(EVRDLYGENARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrout.r.iocsh" "P=$(PEVR),$(EVROUTARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrin.r.iocsh" "P=$(PEVR),$(EVRINARGS=)"

# [user] settings - use this file to apply local customization AFTER INIT
iocshLoad "$(TOP)/user-after-init.iocsh" "P=$(PEVR)"
