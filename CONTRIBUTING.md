
## iocrc.iocsh
Chess (https://chess.esss.lu.se) and node specific settings
```
iocshLoad "$(TOP)/iocrc.iocsh"
```
### Contribution
* Modify the Chess entries in order to get the update here.

## user.iocsh
Your local customizations are imported within 'user.iocsh' for your host.
```
iocshLoad "$(TOP)/user.iocsh"             "P=$(PEVR)"
```
### Contribution
* Upload your custom "user.iocsh" file under 'inventory' of this repository.
* File format: ${hostname}-user.iocsh

## st.cmd and *.iocsh modifications 

Slack the maintainers if something can be done better. Or upload to this repository.

## Conclusions
The contributions will be incorporated into the future releases.
